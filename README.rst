WS Control
==========

Web front-end for `WebSupport API`_.

Only DNS management is implemented at the moment.

Development status: **Alpha**

**NOTE:** WS Control was developed with Python 3.4, though it should run
on any Python 3.

.. _WebSupport API: https://rest.websupport.sk/docs/index


Locations
---------

The `project page`_ is hosted on Bitbucket.

If you find something wrong or know of a missing feature, please
`create an issue`_ on the project page.

.. _project page:         https://bitbucket.org/beli-sk/wscontrol
.. _create an issue:      https://bitbucket.org/beli-sk/wscontrol/issues


Structure overview
------------------

::

   .
   |-- docker-compose.yml
   |-- README.rst
   `-- wscontrol/ ............. base of the Docker container definition
       |-- Dockerfile
       |-- requirements.txt
       `-- wscontrol/ ......... Python 'package' of the flask app
           |-- __init__.py
           |-- views.py
           |-- templates/
           |   |-- base.html
           |   |-- login.html
           |   |-- zone.html
           |   `-- zones.html
           `-- wsapi/ ......... sub-package providing API bindings
               |-- __init__.py
               |-- ctrl.py
               |-- dns.py
               `-- utils.py


Install
-------

You need to have Docker_ and `Docker Compose`_ already installed.

Clone the Git repository::

   git clone https://bitbucket.org/beli-sk/wscontrol.git
   cd wscontrol/

Then build and run the container::

   docker-compose build
   docker-compose up

**Or** start locally (not tested much, virtualenv recommended but out of scope of this guide))::

   cd wscontrol/
   pip install -r requirements.txt
   ./run.py

After that you should be able to reach the application at http://localhost:8080 from your browser.

.. _Docker: https://www.docker.com
.. _Docker Compose: https://docs.docker.com/compose/


License
-------

Copyright 2016 Michal Belica <devel@beli.sk>

::

    WS Control is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    WS Control is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with WS Control.  If not, see < http://www.gnu.org/licenses/ >.

A copy of the license can be found in the ``LICENSE`` file in the
distribution.

