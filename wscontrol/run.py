#!/usr/bin/env python3

from wscontrol import app
app.run(debug=True, host='0.0.0.0', port=8080)
