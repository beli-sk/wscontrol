import os

from flask import Flask


app = Flask(__name__)

app.config.update(
    DEBUG=True,
    URI_PREFIX='',
    SECRET_KEY='zirafa',
    #SECRET_KEY=os.urandom(24),
    HEADLINE='WebSupport Control'
    )

if app.config.get('DEBUG'):
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)

# finally import views
import wscontrol.views

