class ApiObject(dict):
    """Base class for API objects

    Based on and behaved like a dict with item modification tracking, better
    text representation and possibly (not yet implemented) validation.
    """

    attrs = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.modified = set()

    def __setitem__(self, key, value):
        modified = False
        if isinstance(key, str) and value != self[key]:
            modified = True
        else:
            modified = False
        super().__setitem__(key, value)
        if modified:
            self.modified.add(key)

    def copy(self):
        # recreate a new dict based on self
        new = self.__class__(self)
        # and copy custom attributes
        new.modified = self.modified.copy()
        return new

    def __repr__(self):
        return '{}("{}")'.format(self.__class__.__name__, str(self))

    def __str__(self):
        return super().__repr__()


class DNSZone(ApiObject):

    attrs = ['name']

    def __str__(self):
        return self.get('name', '')


class DNSRecord(ApiObject):
    """DNS record

    Returned by API calls or created like a regular dict:

    >>> DNSRecord(name='localhost', type='A', content='127.0.0.1')

    or

    >>> DNSRecord({'name': 'localhost', 'type': 'A', 'content': '127.0.0.1')
    """

    attrs = ['name', 'ttl', 'type', 'prio', 'weight', 'port', 'content']

    def __str__(self):
        return ' '.join((str(self.get(x)) for x in self.attrs if self.get(x)))


class DNSManager(object):

    def __init__(self, wsapi):
        self.wsapi = wsapi

    def list_zones(self):
        """List zones

        Returns list of DNSZone objects accessible under logged in account.
        """

        status, reason, data = self.wsapi.do_request('zone')
        zones = list()
        for item in data['items']:
            zones.append(DNSZone(item))
        return zones

    def list_records(self, zone):
        """List DNS records

        List records under zone. Zone parameter can be either string or DNSZone
        object.
        """
        status, reason, data = self.wsapi.do_request(
                'zone/{}/record'.format(zone))
        records = list()
        for item in data['items']:
            records.append(DNSRecord(item))
        return records

    def get_record(self, zone, record_id):
        """Get record by ID
        """
        status, reason, data = self.wsapi.do_request(
                'zone/{}/record/{}'.format(zone, record_id), method='GET')
        return DNSRecord(data)

    def add_record(self, zone, record):
        """Add record

        Record should be a DNSRecord object to add under zone.
        """
        status, reason, data = self.wsapi.do_request(
                'zone/{}/record'.format(zone), data=record, method='POST')
        return data['status'], data['errors'], DNSRecord(data['item'])

    def update_record(self, zone=None, record=None):
        if record is None:
            raise Exception('no record to update')
        if zone is None:
            zone = DNSZone(record['zone'])
        xrecord = DNSRecord(id=record['id'])
        for key in record.modified:
            xrecord[key] = record[key]
        status, reason, data = self.wsapi.do_request(
                'zone/{}/record/{}'.format(zone, record['id']),
                data=xrecord, method='PUT')

    def del_record(self, zone, record):
        if isinstance(record, DNSRecord):
            record_id = DNSRecord['id']
        else:
            record_id = record
        status, reason, data = self.wsapi.do_request('zone/{}/record/{}'.format(zone, record_id), method='DELETE')

