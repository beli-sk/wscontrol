import json
import urllib.request

from .utils import PreemptiveBasicAuthHandler
from .dns import DNSManager

class WSApi(object):
    """WebSupport API bindings

    usage example:

    >>> from wsapi import WSApi
    >>> ws = WSApi(user, password)
    >>> ws.dns.list_zones()
    [DNSZone("n5zrgke.sk")]
    """

    base_uri_tmpl = 'https://rest.websupport.sk/v1/user/self/'

    @property
    def base_uri(self):
        return self.base_uri_tmpl.format(user=self.user)

    def __init__(self, user=None, password=None):
        if user is not None:
            self.user = user
        if password is not None:
            self.password = password
        self.dns = DNSManager(self)

    def do_request(self, path, method='GET', data=None):
        """Send HTTP request

        path is relative to base_uri.

        Returns tuple (status, reason, re_data):
        status .... HTTP status code
        reason .... HTTP status text returned by server
        re_data ... data structure de-serialized from JSON response
        """
        if self.user and self.password is not None:
            auth_handler = PreemptiveBasicAuthHandler()
            auth_handler.add_password(
                    realm=None,
                    uri=self.base_uri,
                    user=self.user,
                    passwd=self.password,
                    )
            opener = urllib.request.build_opener(auth_handler)
        else:
            opener = urllib.request.build_opener()
        if data:
            data_str = json.dumps(data).encode('utf-8')
        else:
            data_str = None
        request = urllib.request.Request(self.base_uri + path, data=data_str, method=method)
        request.add_header('accept', 'application/json')
        if data_str:
            request.add_header('content-type', 'application/json; charset=utf-8')
        with opener.open(request) as f:
            re_data_str = f.read().decode('utf-8')
        if re_data_str:
            re_data = json.loads(re_data_str)
        return f.status, f.reason, re_data

