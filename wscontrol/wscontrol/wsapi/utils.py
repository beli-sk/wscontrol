import urllib.request
import base64


class PreemptiveBasicAuthHandler(urllib.request.HTTPBasicAuthHandler):
    '''Preemptive basic auth.

    Instead of waiting for a 40x to then retry with the credentials,
    send the credentials if the url is handled by the password manager.
    Note: please use realm=None when calling add_password.
    Inspired by http://stackoverflow.com/a/24048772
    Updated for Python 3 by Beli'''
    def http_request(self, req):
        url = req.get_full_url()
        realm = None
        user, pw = self.passwd.find_user_password(realm, url)
        if pw:
            raw = "%s:%s" % (user, pw)
            auth = 'Basic %s' % base64.b64encode(raw.encode('ascii')).strip().decode('ascii')
            req.add_unredirected_header(self.auth_header, auth)
        return req

    https_request = http_request

