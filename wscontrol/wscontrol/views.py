from urllib.error import HTTPError

from flask import Flask, redirect, url_for, request, session, g, render_template, flash
from flask.views import MethodView

from . import app
from .wsapi import WSApi
from .wsapi.dns import DNSRecord


@app.context_processor
def extra_context():
    return dict(
        headline=app.config['HEADLINE'],
        user = session.get('user')
        )


class AuthenticatedView(MethodView):
    """Base class for authenticated pages

    Redirects to login if no user is authenticated.
    """
    def dispatch_request(self, *a, **kw):
        if 'user' in session:
            return super().dispatch_request(*a, **kw)
        else:
            return redirect(url_for('login'))


class IndexView(MethodView):

    def get(self):
        if 'user' not in session:
            return redirect(url_for('login'))
        else:
            return redirect(url_for('zones'))


class LoginView(MethodView):

    def get(self):
        return render_template('login.html', subheadline='Log-in')

    def post(self):
        user = request.form['user']
        password = request.form['password']
        # try an API request to verify credentials
        try:
            WSApi(user, password).dns.list_zones()
        except HTTPError as e:
            flash('API error: {} {}'.format(e.code, e.reason), 'danger')
            return redirect(url_for('login'))
        session['user'] = user
        session['pw'] = password
        return redirect(url_for('zones'))


class LogoutView(MethodView):

    def get(self):
        session.pop('user', None)
        session.pop('pw', None)
        flash('Logged out.', 'success')
        return redirect(url_for('login'))


class ZonesView(AuthenticatedView):

    def get(self):
        zones = WSApi(session['user'], session['pw']).dns.list_zones()
        return render_template('zones.html', subheadline='DNS zones', zones=zones)


class ZoneView(AuthenticatedView):

    def get(self, zone):
        try:
            records = WSApi(session['user'], session['pw']).dns.list_records(zone)
        except HTTPError as e:
            flash('API error: {} {}'.format(e.code, e.reason), 'danger')
            return redirect(url_for('zones'))
        form_errors = session.pop('add_record_errors', None)
        form_content = session.pop('add_record_content', None)
        return render_template(
                'zone.html', subheadline='DNS zone {}'.format(zone),
                records=records, errors=form_errors, content=form_content)

    def post(self, zone):
        if 'add' in request.form.keys():
            # add button pressed
            # construct DNSRecord with keys from form data contained in DNSRecord.attrs
            record = DNSRecord([x for x in request.form.items() if x[1] and x[0] in DNSRecord.attrs])
            try:
                status, errors, added_record = WSApi(session['user'], session['pw']).dns.add_record(zone, record)
            except HTTPError as e:
                flash('API error: {} {}'.format(e.code, e.reason), 'danger')
                return redirect(url_for('zone', zone=zone))
            if errors:
                # save validation errors in session, converting list of values into single string
                session['add_record_errors'] = dict([(k, ' '.join(v)) for k, v in errors.items()])
                session['add_record_content'] = record
                flash('Add record failed ({}).'.format(status), 'danger')
            else:
                flash('Record added ({}).'.format(added_record), 'success')
            return redirect(url_for('zone', zone=zone))
        else:
            # find form field names starting with 'del:'
            del_list = [x[4:] for x in request.form.keys() if x[:4] == 'del:']
            if del_list:
                # del button pressed
                try:
                    WSApi(session['user'], session['pw']).dns.del_record(zone, del_list[0])
                except HTTPError as e:
                    flash('API error: {} {}'.format(e.code, e.reason), 'danger')
                    return redirect(url_for('zone', zone=zone))
                flash('Record deleted.', 'success')
                return redirect(url_for('zone', zone=zone))


app.add_url_rule(app.config['URI_PREFIX']+'/', view_func=IndexView.as_view('index'))
app.add_url_rule(app.config['URI_PREFIX']+'/login', view_func=LoginView.as_view('login'))
app.add_url_rule(app.config['URI_PREFIX']+'/logout', view_func=LogoutView.as_view('logout'))
app.add_url_rule(app.config['URI_PREFIX']+'/zones', view_func=ZonesView.as_view('zones'))
app.add_url_rule(app.config['URI_PREFIX']+'/zone/<zone>', view_func=ZoneView.as_view('zone'))

